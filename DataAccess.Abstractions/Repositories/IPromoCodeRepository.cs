﻿using DataAccess.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Abstractions.Repositories
{
    public interface IPromoCodeRepository
    {
        Task<PromoCode> GetPromoCodeByValueAsync(string value);
        Task<PromoCode> AddNewPromoCodeAsync(PromoCode model);
        Task<bool> UpdateAsync(PromoCode item);
    }
}
