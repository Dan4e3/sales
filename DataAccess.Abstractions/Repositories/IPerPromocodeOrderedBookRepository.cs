﻿using DataAccess.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Abstractions.Repositories
{
    public interface IPerPromocodeOrderedBookRepository
    {
        Task<PerPromocodeOrderedBook> CreateAsync(PerPromocodeOrderedBook item);
        Task<List<Book>> GetBooksByPromocodeValueAsync(string promoCodeValue);
        Task<PerPromocodeOrderedBook> GetOrderEntryByBookIdAndPromocodeValueAsync(long bookId, string promoCodeValue);
        Task<bool> DeleteItemAsync(PerPromocodeOrderedBook item);
        Task<bool> CheckIfBookAlreadyOrderedAsync(long bookId, string promoCodeValue);
    }
}
