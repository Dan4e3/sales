﻿using DataAccess.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Abstractions.Repositories
{
    public interface IBookRepository
    {
        Task<long> AddBookAsync(Book item);
        Task<bool> DeleteBookByIdAsync(long bookId);
        Task<List<Book>> GetBooksPagedAsync(int pageNum, int batchSize);
        Task<Book> GetBookByIdAsync(long id);
        Task<int> GetTotalBooksCountAsync();
        IQueryable<Book> GetAllBooksAsQueryable();
        Task<bool> UpdateAsync(Book item);
    }
}
