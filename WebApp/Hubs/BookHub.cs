﻿using AutoMapper;
using DataAccess.Abstractions.Repositories;
using DataAccess.Core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models.ViewModels;

namespace WebApp.Hubs
{
    public class BookHub: Hub
    {
        IPerPromocodeOrderedBookRepository _perPromoCodeBookRepo;
        IBookRepository _bookRepo;
        IHttpContextAccessor _httpContextAccessor;
        IMapper _mapper;

        public BookHub(IPerPromocodeOrderedBookRepository perPromoCodeBookRepo,
            IHttpContextAccessor httpContextAccessor, IBookRepository bookRepo, IMapper mapper)
        {
            _perPromoCodeBookRepo = perPromoCodeBookRepo;
            _httpContextAccessor = httpContextAccessor;
            _bookRepo = bookRepo;
            _mapper = mapper;
        }

        public async Task ReduceBookQuantity(BookViewModel bookVm)
        {
            if (await _perPromoCodeBookRepo.CheckIfBookAlreadyOrderedAsync(bookVm.Id, _httpContextAccessor.HttpContext.User.Identity.Name))
                return;

            if (bookVm.QuantityLeft > 0)
                bookVm.QuantityLeft--;

            await this.Clients.All.SendAsync("ReduceBookQuantity", bookVm);
        }

        public async Task IncreaseBookQuantity(ShoppingCartItemViewModel cartItemVm)
        {
            Book book = await _bookRepo.GetBookByIdAsync(cartItemVm.Id);
            BookViewModel bookVm = _mapper.Map<BookViewModel>(book);

            bookVm.QuantityLeft++;

            await this.Clients.All.SendAsync("IncreaseBookQuantity", bookVm);
        }
    }
}
