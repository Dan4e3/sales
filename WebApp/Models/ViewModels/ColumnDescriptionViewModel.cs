﻿using BusinessLogic;
using DataAccess.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace WebApp.Models.ViewModels
{
    public class ColumnDescriptionViewModel
    {
        public string Name { get; set; }
        public string DataType { get; set; }
        public bool ShouldBeDisplayed { get; set; }

        public static IEnumerable<ColumnDescriptionViewModel> GetColumnInfos<T>()
        {
            var props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);
            return props.Select(x => {
                var displayNameAttr = x.GetCustomAttribute<ColumnDisplayNameAttribute>();
                var name = displayNameAttr == null ? x.Name : displayNameAttr.Name;

                var dataTypeAttr = x.GetCustomAttribute<ColumnDataTypeAttribute>();
                var dataType = dataTypeAttr == null ? ColumnDataTypes.Text : dataTypeAttr.TypeName;

                return new ColumnDescriptionViewModel()
                {
                    Name = name,
                    DataType = dataType,
                    ShouldBeDisplayed = displayNameAttr == null ? false : true
                };
            });
        }
    }
}
