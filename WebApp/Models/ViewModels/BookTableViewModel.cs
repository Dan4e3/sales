﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.ViewModels
{
    public class BookTableViewModel
    {
        public IEnumerable<BookViewModel> Books { get; set; }
        public IEnumerable<ColumnDescriptionViewModel> ColumnInfos { get; set; }
        public int DisplayPerPage { get; set; }
        public int PagesCount { get; set; }
    }
}
