﻿using BusinessLogic;
using DataAccess.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace WebApp.Models.ViewModels
{
    public class BookViewModel
    {
        public long Id { get; set; }
        [ColumnDisplayName("Название")]
        public string Title { get; set; }

        [ColumnDisplayName("ISBN")]
        public string Isbn { get; set; }

        [ColumnDisplayName("Обложка")]
        [ColumnDataType(ColumnDataTypes.Image)]
        public string CoverImageFileName { get; set; }

        [ColumnDisplayName("Стоимость")]
        public decimal Price { get; set; }

        [ColumnDisplayName("Осталось шт.")]
        public int QuantityLeft { get; set; }

        [ColumnDisplayName("Автор")]
        public string AuthorFullName { get; set; }
    }
}
