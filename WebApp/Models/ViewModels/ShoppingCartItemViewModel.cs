﻿using DataAccess.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.ViewModels
{
    public class ShoppingCartItemViewModel
    {
        public long Id { get; set; }

        [ColumnDisplayName("Название")]
        public string Title { get; set; }

        [ColumnDisplayName("Стоимость")]
        public decimal Price { get; set; }

        [ColumnDisplayName("Автор")]
        public string AuthorFullName { get; set; }
    }
}
