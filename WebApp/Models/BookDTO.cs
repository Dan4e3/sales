﻿using DataAccess.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class BookDTO
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public AuthorDTO Author { get; set; }
        public string Isbn { get; set; }
        public string CoverImageFileName { get; set; }
        public decimal Price { get; set; }
        public int QuantityLeft { get; set; }
    }
}
