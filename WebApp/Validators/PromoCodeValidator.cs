﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models.ViewModels;

namespace WebApp.Validators
{
    public class PromoCodeValidator: AbstractValidator<PromoCodeViewModel>
    {
        public PromoCodeValidator()
        {
            RuleFor(x => x.Value)
                .Length(32)
                .WithMessage("Промокод должен состоять из 32 симовлов!");
        }
    }
}
