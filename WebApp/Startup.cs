using BusinessLogic.PromoCode;
using BusinessLogic.ShoppingCart;
using DataAccess.Abstractions;
using DataAccess.Abstractions.Repositories;
using DataAccess.Core;
using DataAccess.Core.Models;
using DataAccess.Implementations.Repositories;
using FluentValidation.AspNetCore;
using JavaScriptEngineSwitcher.Extensions.MsDependencyInjection;
using JavaScriptEngineSwitcher.ChakraCore;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using React.AspNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using WebApp.MappingProfiles;
using WebApp.Models;
using BusinessLogic.Abstractions;
using BusinessLogic.Auth;
using WebApp.Hubs;
using Newtonsoft.Json;

namespace WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<SalesContext>(opts =>
                opts.UseSqlServer(Configuration.GetConnectionString("salesDB"))
                );

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = new PathString("/Auth/Login");
                });

            services.AddAutoMapper(typeof(SalesMappingProfile));

            services.AddSession();

            services.AddSignalR(opts => opts.EnableDetailedErrors = true);

            services.AddMemoryCache();

            services.AddScoped<IPromoCodeRepository, PromoCodeRepository>();
            services.AddScoped<IBookRepository, BookRepository>();
            services.AddScoped<IPerPromocodeOrderedBookRepository, PerPromocodeOrderedBookRepository>();
            services.AddScoped<IShoppingCart<Book>, BookShoppingCart>();
            services.AddScoped<IAuthentication, CookieAuthentication>();
            services.AddSingleton<IPromoCodeGenerator, GuidPromoCodeGenerator>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddReact();
            services.AddJsEngineSwitcher(options => 
                options.DefaultEngineName = ChakraCoreJsEngine.EngineName)
              .AddChakraCore();

            services.AddControllersWithViews()
                .AddNewtonsoftJson(options => {
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                })
                .AddFluentValidation(opts => 
                    opts.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly()));

            services.AddSwaggerGen();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, SalesContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger(options =>
                {
                    options.SerializeAsV2 = true;
                });
                app.UseSwaggerUI();

                context.Database.EnsureCreated();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseReact(config => {
                config.SetLoadBabel(false)
                      .SetLoadReact(false)
                      .SetReactAppBuildPath("~/dist")
                      .SetUseDebugReact(true);
            });
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseSession();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<BookHub>("/booksReducer");
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
