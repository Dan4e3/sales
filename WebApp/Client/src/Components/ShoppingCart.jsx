﻿import { Component } from 'react';
import React from 'react';
import { TableHeader, TableRow } from './BookTable.jsx';



export default class ShoppingCartComponent extends Component {

    constructor(props) {
        super(props);

        this.handleRowClick = this.handleRowClick.bind(this);
    }

    componentDidUpdate(prevProps) {
        if (this.props.hubConnection !== prevProps.hubConnection) {
            this.props.hubConnection.on("ReduceBookQuantity", (bookModel) => {
                let indexToReplace = this.state.rows.findIndex((row) => row.id === bookModel.id);
                let rows = [...this.state.rows];
                rows[indexToReplace] = bookModel;
                this.setState({ rows });
            });
        }
    }

    handleRowClick(clickedCartItem) {
        this.props.hubConnection
            .invoke("IncreaseBookQuantity", clickedCartItem)
            .catch(err => console.error(err));

        this.props.itemRemovedFromCart(clickedCartItem);
    }

    render() {
        const tableRows = (this.props.shoppingCartRows.map(row => {
            return <TableRow key={row.id} rowClicked={this.handleRowClick} row={row} columnInfos={this.props.columnInfos} />
        }));
        
        return (
            <table className="table table-striped table-hover align-middle">
                <thead>
                    <tr>
                        <TableHeader columnInfos={this.props.columnInfos} />
                    </tr>
                </thead>
                <tbody>
                    {tableRows}
                </tbody>
            </table>
            );
    }
}