﻿import { Component } from 'react';
import ReactDOM from 'react-dom';
import React from 'react';
import BookTable from './BookTable.jsx';
import ShoppingCart from './ShoppingCart.jsx';
import * as signalR from '@microsoft/signalr';

export default class HomeComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            shoppingCartRows: [],
            cartColumnInfos: [],
            hubConnection: null,
            shoppingCartTotalSum: 0,
            minimumSumThreshold: 0,
            promocodeUsed: false
        };

        this.handleItemAddedToCart = this.handleItemAddedToCart.bind(this);
        this.handleItemRemovedFromCart = this.handleItemRemovedFromCart.bind(this);
        this.handleSubmitOrder = this.handleSubmitOrder.bind(this);
    }

    getSumOfItemsInCart(cartItems) {
        return cartItems.map(item => item.price)
            .reduce((a, b) => a + b, 0);
    }

    componentDidMount() {
        fetch('/ShoppingCart/InitializeCart')
            .then(response => response.json())
            .then(result => {
                this.setState({
                    shoppingCartRows: result.cartItems,
                    cartColumnInfos: result.columnInfos,
                    shoppingCartTotalSum: this.getSumOfItemsInCart(result.cartItems),
                    minimumSumThreshold: result.minimumSumThreshold,
                    promocodeUsed: result.promocodeUsed
                });
            });

        const hubConnection = new signalR.HubConnectionBuilder()
            .withUrl("https://localhost:44380/booksReducer")
            .build();

        this.setState({ hubConnection }, () => {
            this.state.hubConnection
                .start()
                .then(() => console.log("Hub connected!"))
                .catch(err => console.log(`Error occured on hub connection establish: ${err}`));
        });
    }

    handleItemAddedToCart(bookRowClicked) {
        fetch(`/ShoppingCart/Add/?bookId=${bookRowClicked.id}`)
            .then(response => {
                if (response.ok)
                    return response.json();
                throw response;
            })
            .then(result => {
                this.setState({
                    shoppingCartRows: result.cartItems,
                    shoppingCartTotalSum: this.getSumOfItemsInCart(result.cartItems)
                });
            })
            .catch(err => err.text().then(errMessage =>
                alert(errMessage)
            ));
    }

    handleItemRemovedFromCart(cartItemClicked) {
        fetch(`/ShoppingCart/Remove/?bookId=${cartItemClicked.id}`)
            .then(response => {
                if (response.ok)
                    return response.json();
                throw response;
            })
            .then(result => {
                this.setState({
                    shoppingCartRows: result.cartItems,
                    shoppingCartTotalSum: this.getSumOfItemsInCart(result.cartItems)
                });
            })
            .catch(err => err.text().then(errMessage =>
                alert(errMessage)
            ));
    }

    handleSubmitOrder() {
        fetch('/ShoppingCart/SubmitOrder')
            .then(response => {
                if (response.ok)
                    return response.json();
                throw response
            })
            .then(result => {
                this.setState({
                    promocodeUsed: result.promocodeUsed
                });
            })
            .catch(err => err.text().then(errMessage =>
                alert(errMessage)
            ));
    }

    render() {
        const readyToSubmitOrder = this.state.shoppingCartTotalSum >= this.state.minimumSumThreshold ? true : false;
        if (!this.state.promocodeUsed)
            return (
                <div className="container-fluid">
                    <div className="row">
                        <div className="col">
                            <BookTable itemAddedToCart={this.handleItemAddedToCart} hubConnection={this.state.hubConnection} />
                        </div>
                        <div className="col">
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col">
                                        <ShoppingCart itemRemovedFromCart={this.handleItemRemovedFromCart} shoppingCartRows={this.state.shoppingCartRows}
                                            columnInfos={this.state.cartColumnInfos} hubConnection={this.state.hubConnection} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <span>Итоговая сумма: <b>{(this.state.shoppingCartTotalSum).toFixed(2)}</b></span>
                                    </div>
                                    <div className="col">
                                        <button disabled={!readyToSubmitOrder} onClick={this.handleSubmitOrder}>Оформить заказ</button>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        {!readyToSubmitOrder ? <p>Минимальная сумма заказа - {this.state.minimumSumThreshold} руб.</p> : null}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        else
            return (
                <div>
                    <h2>Заказ успешно оформлен! Промокод использован!</h2>
                </div>
            )
    }
}

const renderElement = document.getElementById("root-home");
if (renderElement)
    ReactDOM.render(<HomeComponent />, renderElement);