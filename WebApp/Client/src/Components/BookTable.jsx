﻿import { Component } from 'react';
import React from 'react';


export class TableHeader extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const app = (
            this.props.columnInfos.map((colInfo, index) => {
                if (colInfo.shouldBeDisplayed)
                    return <th key={index}>{colInfo.name}</th>
            })
        );
        return app;
    }
}

export class TableRow extends Component {

    constructor(props) {
        super(props);

        this.handleRowClick = this.handleRowClick.bind(this);
    }

    handleRowClick(e) {
        this.props.rowClicked(this.props.row);
    }

    render() {
        const row = this.props.row;
        const columnInfos = this.props.columnInfos;
        const app = (
            <tr key={row.id} className="clickable-tablerow" onClick={this.handleRowClick}>
                {Object.keys(row).map((key, index) => {
                    if (columnInfos[index].shouldBeDisplayed) {
                        let displayValue = columnInfos[index].dataType == "Image" ?
                            <img src={"/images/" + row[key]} className="image-cover" /> : row[key];
                        return <td key={index}>{displayValue}</td>
                    }
                })}
            </tr>
            );

        return app;
    }
}

class PageLink extends Component {

    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        e.preventDefault();
        this.props.onPageLinkClick(this.props.pageNumber);
    }

    render() {
        let displayValue;
        if (this.props.selected)
            displayValue = <b>{this.props.pageNumber}</b>;
        else
            displayValue = this.props.pageNumber;

        return (
            <a href="#" className="page-link" onClick={this.handleClick}>{displayValue}</a>
            )
    }
}

export default class BookTableComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            displayPerPage: 0,
            currentPage: 1,
            totalPages: 0,
            rows: [],
            columnInfos: []
        };

        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.handlePageLinkClick = this.handlePageLinkClick.bind(this);
        this.handleRowClick = this.handleRowClick.bind(this);
    }

    componentDidUpdate(prevProps) {
        if (this.props.hubConnection !== prevProps.hubConnection) {
            this.props.hubConnection.on("ReduceBookQuantity", (bookModel) => {
                let indexToReplace = this.state.rows.findIndex((row) => row.id === bookModel.id);
                let rows = [...this.state.rows];
                rows[indexToReplace] = bookModel;
                this.setState({ rows });
            });

            this.props.hubConnection.on("IncreaseBookQuantity", (bookModel) => {
                let indexToReplace = this.state.rows.findIndex((row) => row.id === bookModel.id);
                let rows = [...this.state.rows];
                rows[indexToReplace] = bookModel;
                this.setState({ rows });
            });
        }
    }

    componentDidMount() {
        fetch(`/Home/InitializeBooksTable`)
            .then(response => response.json())
            .then(result => {
                this.setState({
                    rows: result.rows,
                    columnInfos: result.columnInfos,
                    displayPerPage: result.displayPerPage,
                    totalPages: result.pagesCount
                })
            }
        );
    }

    handleSelectChange(event) {
        this.setState({
            displayPerPage: event.target.value,
        });

        this.fetchNewData(1, event.target.value);
    }

    handlePageLinkClick(pageNum) {
        this.fetchNewData(pageNum, this.state.displayPerPage);
    }

    handleRowClick(clickedRow) {
        this.props.hubConnection
            .invoke("ReduceBookQuantity", clickedRow)
            .catch(err => console.error(err));

        this.props.itemAddedToCart(clickedRow);
    }

    fetchNewData(pageNum, pageSize) {
        fetch(`/home/GetBooksAtPage/?page=${pageNum}&pageSize=${pageSize}`)
            .then(response => response.json())
            .then(result => {
                this.setState({
                    rows: result.rows,
                    totalPages: result.totalPages,
                    currentPage: pageNum
                });
            }
            )
    }

    render() {
        const tableRows = (this.state.rows.map(row =>
            <TableRow key={row.id} rowClicked={this.handleRowClick} row={row} columnInfos={this.state.columnInfos} />));

        const app = (
            <div className="containter-fluid">
                <div className="row">
                    <table className="table table-hover table-striped align-middle ">
                        <thead>
                            <tr>
                                <TableHeader columnInfos={this.state.columnInfos} />
                            </tr>
                        </thead>
                        <tbody>
                            {tableRows}
                        </tbody>
                    </table>
                </div>
                <div className="row">
                    <div className="col">
                        <nav aria-label="Books table paginator">
                            <ul className="pagination">
                                {Array.apply(null, Array(this.state.totalPages)).map(function (x, i) {
                                    let p = i + 1;
                                    return (
                                        <li key={p} className="page-item">
                                            <PageLink key={p} selected={this.state.currentPage === p ? true : false}
                                                pageNumber={p} onPageLinkClick={this.handlePageLinkClick} />
                                        </li>
                                    )
                                }, this)}
                            </ul>
                        </nav>
                    </div>
                    <label className="col">
                        Количество книг на странице:
                        <select className="form-select" value={this.state.displayPerPage} onChange={this.handleSelectChange}>
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                        </select>
                    </label>
                </div>
            </div>

        );

        return app;
    }
}