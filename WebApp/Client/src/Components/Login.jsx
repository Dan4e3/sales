﻿import { Component } from 'react';
import React from 'react';
import ReactDOM from 'react-dom';

export default class LoginComponent extends Component {

    constructor(props) {
        super(props);

        this.handleSubmitGetNewPromoCode = this.handleSubmitGetNewPromoCode.bind(this);
    }

    handleSubmitGetNewPromoCode(event) {
        window.location.href = "/Auth/GetPromocode";
    }

    render() {
        return (
            <div>
                <form className="row g-3" method="post" action="/Auth/Login">
                    <div className="col-auto d-flex justify-content-center">
                        <label>
                            Введите промокод:
                            <input type="text" name="Value" />
                        </label>
                    </div>
                    <div className="col-auto d-flex justify-content-center">
                        <input type="submit" value="Войти" />
                    </div>

                    <div className="col-auto d-flex justify-content-center">
                        <button type="button" onClick={this.handleSubmitGetNewPromoCode}>Получить новый промокод!</button>
                    </div>
                </form>
            </div>

        )
    }
}

const renderElement = document.getElementById("root-login");
if (renderElement)
    ReactDOM.render(<LoginComponent />, renderElement);