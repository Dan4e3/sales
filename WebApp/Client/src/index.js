﻿import React from 'react';
import ReactDOM from 'react-dom';
import ReactDOMServer from 'react-dom/server';

import HomeComponent from './Components/Home.jsx';
import LoginComponent from './Components/Login.jsx';


global.React = React;
global.ReactDOM = ReactDOM;
global.ReactDOMServer = ReactDOMServer;

global.Components = { HomeComponent, LoginComponent };