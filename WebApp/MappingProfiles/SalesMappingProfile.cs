﻿using AutoMapper;
using DataAccess.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;
using WebApp.Models.ViewModels;

namespace WebApp.MappingProfiles
{
    public class SalesMappingProfile: Profile
    {
        public SalesMappingProfile()
        {
            CreateMap<PromoCode, PromoCodeViewModel>();
            CreateMap<Book, BookViewModel>()
                .ForMember(dest => dest.AuthorFullName, opts => opts.MapFrom(src => GetAuthorFullName(src) ));

            CreateMap<Book, ShoppingCartItemViewModel>()
                .ForMember(dest => dest.AuthorFullName, opts => opts.MapFrom(src => GetAuthorFullName(src)));

            CreateMap<Author, AuthorDTO>();
            CreateMap<Book, BookDTO>();
            CreateMap<AuthorDTO, Author>();
            CreateMap<BookDTO, Book>();
        }

        private string GetAuthorFullName(Book book)
        {
            return $"{book.Author.LastName}" +
                $" {(book.Author.FirstName.Length > 0 ? book.Author.FirstName[0] + "." : null)}" +
                $" {(book.Author.MiddleName.Length > 0 ? book.Author.MiddleName[0] + "." : null)}";
        }
    }
}
