﻿using AutoMapper;
using BusinessLogic;
using BusinessLogic.Abstractions;
using DataAccess.Abstractions;
using DataAccess.Abstractions.Repositories;
using DataAccess.Core;
using DataAccess.Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models.ViewModels;

namespace WebApp.Controllers
{
    public class ShoppingCartController : Controller
    {
        ILogger<ShoppingCartController> _logger;
        IShoppingCart<Book> _bookShoppingCart;
        IMapper _mapper;
        IBookRepository _bookRepo;
        IPromoCodeRepository _promoCodeRepo;
        IPerPromocodeOrderedBookRepository _perPromocodeBookRepo;

        const decimal _minimumSumThreshold = 2000;

        public ShoppingCartController(ILogger<ShoppingCartController> logger, IShoppingCart<Book> bookShoppingCart,
            IMapper mapper, IBookRepository bookRepo, IPerPromocodeOrderedBookRepository perPromocodeBookRepo,
            IPromoCodeRepository promoCodeRepo)
        {
            _logger = logger;
            _bookShoppingCart = bookShoppingCart;
            _mapper = mapper;
            _bookRepo = bookRepo;
            _perPromocodeBookRepo = perPromocodeBookRepo;
            _promoCodeRepo = promoCodeRepo;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> InitializeCart()
        {
            try
            {
                string promoCodeValue = HttpContext.User.Identity.Name;
                var promoCode = await _promoCodeRepo.GetPromoCodeByValueAsync(promoCodeValue);
                var books = await _perPromocodeBookRepo.GetBooksByPromocodeValueAsync(promoCodeValue);
                var cartItems = _mapper.Map<IEnumerable<ShoppingCartItemViewModel>>(books);

                return new JsonResult(new
                {
                    cartItems = cartItems,
                    columnInfos = ColumnDescriptionViewModel.GetColumnInfos<ShoppingCartItemViewModel>(),
                    minimumSumThreshold = _minimumSumThreshold,
                    promocodeUsed = promoCode.Used
                });
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Возникла ошибка при инициализации корзины");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Add(long bookId)
        {
            try
            {
                Book book = await _bookRepo.GetBookByIdAsync(bookId);
                string promocodeValue = HttpContext.User.Identity.Name;

                if (book.QuantityLeft == 0)
                    return BadRequest("Экземпляры этой книги закончились!");

                if (await _perPromocodeBookRepo.CheckIfBookAlreadyOrderedAsync(book.Id, promocodeValue))
                    return BadRequest("По одному промокоду можно купить только один экземпляр книги!");

                var orderedBook = await _perPromocodeBookRepo.CreateAsync(new PerPromocodeOrderedBook()
                {
                    Book = book,
                    PromoCode = await _promoCodeRepo.GetPromoCodeByValueAsync(promocodeValue)
                });

                book.QuantityLeft--;
                await _bookRepo.UpdateAsync(book);

                var allBooksInCart = await _perPromocodeBookRepo.GetBooksByPromocodeValueAsync(promocodeValue);
                var mappedAllBooksInCart = _mapper.Map<IEnumerable<ShoppingCartItemViewModel>>(allBooksInCart);

                return Ok(new
                {
                    cartItems = mappedAllBooksInCart
                });
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Ошибка при добавлении книги в корзину");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Remove(long bookId)
        {
            try
            {
                string promocodeValue = HttpContext.User.Identity.Name;
                var orderedBook = await _perPromocodeBookRepo.GetOrderEntryByBookIdAndPromocodeValueAsync(bookId, promocodeValue);

                Book book = orderedBook.Book;
                book.QuantityLeft++;

                await _bookRepo.UpdateAsync(book);
                await _perPromocodeBookRepo.DeleteItemAsync(orderedBook);

                var allBooksInCart = await _perPromocodeBookRepo.GetBooksByPromocodeValueAsync(promocodeValue);
                var mappedAllBooksInCart = _mapper.Map<IEnumerable<ShoppingCartItemViewModel>>(allBooksInCart);

                return Ok(new
                {
                    cartItems = mappedAllBooksInCart
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка при удалении книги из корзины");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> SubmitOrder()
        {
            try
            {
                string promoCodeValue = HttpContext.User.Identity.Name;

                var promoCode = await _promoCodeRepo.GetPromoCodeByValueAsync(promoCodeValue);

                if (promoCode.Used)
                    return BadRequest("Оформить заказ повторно по этому промокоду невозможно!");

                var books = await _perPromocodeBookRepo.GetBooksByPromocodeValueAsync(promoCodeValue);

                if (books.Any(b => b.QuantityLeft < 0))
                    return BadRequest("Некоторые позиции закончили на складе. Пожалуйста, обновите страницу и перепроверьте заказ!");

                if (books.Sum(b => b.Price) < _minimumSumThreshold)
                    return BadRequest($"Минимальная сумма заказа - {_minimumSumThreshold} руб.! Добавьте ещё товаров в корзину");

                promoCode.Used = true;
                await _promoCodeRepo.UpdateAsync(promoCode);

                return Ok(new
                {
                    promocodeUsed = true
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Возникла ошибка при подтверждении заказа");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
