﻿using AutoMapper;
using BusinessLogic;
using DataAccess.Abstractions.Repositories;
using DataAccess.Core;
using DataAccess.Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models.ViewModels;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        ILogger<HomeController> _logger;
        IBookRepository _bookRepo;
        IMapper _mapper;
        const int _perPageCount = 5;

        public HomeController(ILogger<HomeController> logger, IBookRepository bookRepo,
            IMapper mapper)
        {
            _logger = logger;
            _bookRepo = bookRepo;
            _mapper = mapper;
        }

        [HttpGet]
        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetBooksAtPage(int page, int pageSize)
        {
            try
            {
                var books = await PaginatedList<Book>.CreateAsync(_bookRepo.GetAllBooksAsQueryable(), page, pageSize, b => b.Id);
                var mappedBooksVm = _mapper.Map<IEnumerable<BookViewModel>>(books.AsEnumerable());

                return new JsonResult(new
                {
                    rows = mappedBooksVm,
                    totalPages = books.TotalPages,
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Возникла ошибка при получении постраничного списка книг!");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> InitializeBooksTable()
        {
            try
            {
                var books = await PaginatedList<Book>.CreateAsync(_bookRepo.GetAllBooksAsQueryable(), 1, _perPageCount, b => b.Id);
                var mappedBooksVm = _mapper.Map<IEnumerable<BookViewModel>>(books.AsEnumerable());

                return new JsonResult(new
                {
                    rows = mappedBooksVm,
                    columnInfos = ColumnDescriptionViewModel.GetColumnInfos<BookViewModel>(),
                    displayPerPage = _perPageCount,
                    pagesCount = books.TotalPages
                });
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Произошла ошибка при инициализации начального состояния таблицы с книгами");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
