﻿using AutoMapper;
using DataAccess.Abstractions.Repositories;
using DataAccess.Core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;

namespace WebApp.Controllers
{
    [ApiController]
    [Route("api/books")]
    public class ApiController: ControllerBase
    {
        IBookRepository _bookRepo;
        IMemoryCache _memCache;
        IMapper _mapper;
        private string[] _acceptableImgFormats = new string[] { 
            ".jpg", ".jpeg", ".png"
        };

        public ApiController(IBookRepository bookRepo, IMemoryCache memCache, IMapper mapper)
        {
            _bookRepo = bookRepo;
            _memCache = memCache;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<BookDTO>>> GetAllBooks()
        {
            Book[] books;

            if (!_memCache.TryGetValue("books", out books))
            {
                books = await _bookRepo.GetAllBooksAsQueryable().AsNoTracking().ToArrayAsync();
                _memCache.Set("books", books, new MemoryCacheEntryOptions() {  AbsoluteExpirationRelativeToNow = new TimeSpan(0, 1, 0)});
            }

            return Ok(new { books = _mapper.Map<BookDTO[]>(books) });
        }

        [HttpPost]
        public async Task<ActionResult<long>> AddBook(BookDTO bookDto)
        {
            Book book = _mapper.Map<Book>(bookDto);
            var id = await _bookRepo.AddBookAsync(book);
            return Ok(new { id });
        }

        [HttpPost]
        [Route("cover")]
        public async Task<ActionResult<string>> UploadFile(IFormFile file)
        {
            if (!_acceptableImgFormats.Any(x => file.FileName.ToLower().EndsWith(x)))
                return BadRequest($"Неподходящий формат файла. Допустимые форматы: {string.Join(",", _acceptableImgFormats)}");

            string fileName = Path.GetRandomFileName() + ".jpg";
            string path = Path.Combine("wwwroot/images", fileName);
            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
            return Ok(new { 
                savedFileName = fileName
            });
        }

        [HttpPut]
        public async Task<ActionResult<Book>> PatchBook(BookDTO bookDto)
        {
            Book book = _mapper.Map<Book>(bookDto);
            var result = await _bookRepo.UpdateAsync(book);
            if (!result)
                return BadRequest("Ошибка обновления книги");

            return Ok(new { book });
        }

        [HttpDelete]
        public async Task<ActionResult<long>> DeleteBook(long bookId)
        {
            var result = await _bookRepo.DeleteBookByIdAsync(bookId);
            if (!result)
                return BadRequest($"bookId={bookId} не существует!");

            return Ok(new { bookId });
        }
    }
}
