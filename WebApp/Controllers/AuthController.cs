﻿using BusinessLogic.Abstractions;
using DataAccess.Abstractions;
using DataAccess.Abstractions.Repositories;
using DataAccess.Core.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebApp.Models.ViewModels;

namespace WebApp.Controllers
{
    public class AuthController : Controller
    {
        IPromoCodeRepository _promoCodeRepo;
        IPromoCodeGenerator _promoCodeGenerator;
        IAuthentication _authentication;
        ILogger<AuthController> _logger;

        public AuthController(IPromoCodeRepository promoCodeRepo, IPromoCodeGenerator promoCodeGenerator,
            ILogger<AuthController> logger, IAuthentication authentication)
        {
            _promoCodeRepo = promoCodeRepo;
            _promoCodeGenerator = promoCodeGenerator;
            _logger = logger;
            _authentication = authentication;
        }

        [HttpGet]
        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            try
            {
                if (User.Identity.IsAuthenticated)
                    await _authentication.SignOutAsync();

                return RedirectToAction("Login", "Auth");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка при попытке выхода из системы!");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetPromocode()
        {
            try
            {
                _logger.LogInformation("Генерируется новый промокод...");
                string promoCodeValue = _promoCodeGenerator.GetNewPromoCode();
                var promoCode = await _promoCodeRepo.AddNewPromoCodeAsync(new PromoCode() { 
                    Value = promoCodeValue,
                    Used = false
                });

                await _authentication.SignInAsync(promoCodeValue);

                _logger.LogInformation("Промокод успешно сгенерирован!");
                return RedirectToAction("Index", "Home");

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Произошла ошибка при создании нового промокода!");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Login(PromoCodeViewModel promoCodeVm)
        {
            try
            {
                _logger.LogInformation("Осуществляется попытка входа по существующему промокоду...");
                if (ModelState.IsValid)
                {
                    PromoCode promoCode = await _promoCodeRepo.GetPromoCodeByValueAsync(promoCodeVm.Value);

                    if (promoCode == null)
                        ModelState.AddModelError("", "Введённый промокод не существует!");
                    else if (promoCode.Used)
                        ModelState.AddModelError("", "Введённый промокод уже был использован!");
                    else
                    {
                        await _authentication.SignInAsync(promoCode.Value);
                        _logger.LogInformation("Вход успешно произведён!");
                        return RedirectToAction("Index", "Home");
                    }

                }

                return View(promoCodeVm);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Произошла ошибка при попытке аутентификации!");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
