USE [Sales]
GO
SET IDENTITY_INSERT [dbo].[Authors] ON 

INSERT [dbo].[Authors] ([Id], [FirstName], [LastName], [MiddleName]) VALUES (1, N'Ганс', N'Андерсен', N'Христиан')
INSERT [dbo].[Authors] ([Id], [FirstName], [LastName], [MiddleName]) VALUES (4, N'Джорджия', N'Бинг', N'')
INSERT [dbo].[Authors] ([Id], [FirstName], [LastName], [MiddleName]) VALUES (5, N'Михаил', N'Булгаков', N'Афанасьевич')
INSERT [dbo].[Authors] ([Id], [FirstName], [LastName], [MiddleName]) VALUES (3, N'Александр', N'Пушкин', N'Сергеевич')
INSERT [dbo].[Authors] ([Id], [FirstName], [LastName], [MiddleName]) VALUES (2, N'Карл', N'Юнг', N'Густав')
SET IDENTITY_INSERT [dbo].[Authors] OFF
GO
SET IDENTITY_INSERT [dbo].[Books] ON 

INSERT [dbo].[Books] ([Id], [Title], [AuthorId], [ISBN], [CoverImageFileName], [Price], [QuantityLeft]) VALUES (5, N'Гадкий утёнок', 1, N'978-5-389-08354-7', N'gans_andersen/gadkiy_utenok.jpg', CAST(238.16 AS Decimal(10, 2)), 3)
INSERT [dbo].[Books] ([Id], [Title], [AuthorId], [ISBN], [CoverImageFileName], [Price], [QuantityLeft]) VALUES (10, N'Эон. Исследования о символике самости', 2, N'978-5-17-119241-9', N'karl_yung/eon.jpg', CAST(1215.48 AS Decimal(10, 2)), 3)
INSERT [dbo].[Books] ([Id], [Title], [AuthorId], [ISBN], [CoverImageFileName], [Price], [QuantityLeft]) VALUES (11, N'Красная книга', 2, N'978-5-519-60703-2', N'karl_yung/krasnaya_kniga.jpg', CAST(870.00 AS Decimal(10, 2)), 2)
INSERT [dbo].[Books] ([Id], [Title], [AuthorId], [ISBN], [CoverImageFileName], [Price], [QuantityLeft]) VALUES (12, N'Снежная Королева', 1, N'978-5-9268-1543-3', N'gans_andersen/snezhnaya_koroleva.jpg', CAST(305.50 AS Decimal(10, 2)), 12)
INSERT [dbo].[Books] ([Id], [Title], [AuthorId], [ISBN], [CoverImageFileName], [Price], [QuantityLeft]) VALUES (13, N'Девочка со спичками', 1, N'978-5-9985-0335-1', N'gans_andersen/devochka_so_spichkami.jpg', CAST(351.00 AS Decimal(10, 2)), 8)
INSERT [dbo].[Books] ([Id], [Title], [AuthorId], [ISBN], [CoverImageFileName], [Price], [QuantityLeft]) VALUES (14, N'Капитанская дочка', 3, N'978-5-9287-3324-7', N'alexander_pushkin/kapitanskaya_dochka.jpg', CAST(481.11 AS Decimal(10, 2)), 5)
INSERT [dbo].[Books] ([Id], [Title], [AuthorId], [ISBN], [CoverImageFileName], [Price], [QuantityLeft]) VALUES (15, N'Евгений Онегин', 3, N'978-5-699-01973-1', N'alexander_pushkin/evgeniy_onegin.jpg', CAST(401.55 AS Decimal(10, 2)), 9)
INSERT [dbo].[Books] ([Id], [Title], [AuthorId], [ISBN], [CoverImageFileName], [Price], [QuantityLeft]) VALUES (16, N'Сказка о золотом петушке', 3, N'978-5-00041-173-5', N'alexander_pushkin/skazka_o_zolotom_petushke.jpg', CAST(540.00 AS Decimal(10, 2)), 10)
INSERT [dbo].[Books] ([Id], [Title], [AuthorId], [ISBN], [CoverImageFileName], [Price], [QuantityLeft]) VALUES (17, N'Молли Мун и волшебная книга гипноза', 4, N'978-5-389-06220-7', N'georgia_bing/molli_mun_i_volshebnaya_kniga_gipnoza.jpg', CAST(890.15 AS Decimal(10, 2)), 4)
INSERT [dbo].[Books] ([Id], [Title], [AuthorId], [ISBN], [CoverImageFileName], [Price], [QuantityLeft]) VALUES (18, N'Мастер и Маргарита', 5, N'978-5-389-01686-6', N'mikhail_bulgakov/master_i_margarita.jpg', CAST(1005.98 AS Decimal(10, 2)), 21)
INSERT [dbo].[Books] ([Id], [Title], [AuthorId], [ISBN], [CoverImageFileName], [Price], [QuantityLeft]) VALUES (19, N'Собачье сердце', 5, N'978-5-4335-0878-1', N'mikhail_bulgakov/sobachye_serdce.jpg', CAST(700.00 AS Decimal(10, 2)), 14)
SET IDENTITY_INSERT [dbo].[Books] OFF
GO
