﻿using BusinessLogic.Abstractions;
using DataAccess.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.PromoCode
{
    public class GuidPromoCodeGenerator: IPromoCodeGenerator
    {
        public string GetNewPromoCode()
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}
