﻿using DataAccess.Abstractions;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Extensions;
using DataAccess.Core.Models;
using DataAccess.Abstractions.Repositories;
using BusinessLogic.Abstractions;

namespace BusinessLogic.ShoppingCart
{
    public class BookShoppingCart : IShoppingCart<Book>
    {
        IHttpContextAccessor _httpContextAccessor;
        IBookRepository _bookRepo;
        const string _cartKey = "cart";

        public BookShoppingCart(IHttpContextAccessor httpContextAccessor, IBookRepository bookRepo)
        {
            _httpContextAccessor = httpContextAccessor;
            _bookRepo = bookRepo;
        }

        public async Task<List<Book>> AddToShoppingCartAsync(long bookId)
        {
            var session = _httpContextAccessor.HttpContext.Session;

            var bookToAdd = await _bookRepo.GetBookByIdAsync(bookId);
            if (bookToAdd == null)
                return null;

            var booksInCart = session.GetObjectFromJson<List<Book>>(_cartKey);
            if (booksInCart == null)
                booksInCart = new List<Book>();
            booksInCart.Add(bookToAdd);
            session.SetObjectAsJson(_cartKey, booksInCart);
            return booksInCart;
        }

        public List<Book> RemoveFromShoppingCart(long bookId)
        {
            var session = _httpContextAccessor.HttpContext.Session;

            var booksInCart = session.GetObjectFromJson<List<Book>>(_cartKey);
            if (booksInCart == null)
                return null;

            Book bookToRemove = booksInCart.FirstOrDefault(b => b.Id == bookId);
            if (bookToRemove == null)
                return booksInCart;

            booksInCart.Remove(bookToRemove);
            session.SetObjectAsJson(_cartKey, booksInCart);
            return booksInCart;
        }
    }
}
