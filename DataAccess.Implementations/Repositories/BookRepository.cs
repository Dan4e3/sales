﻿using DataAccess.Abstractions.Repositories;
using DataAccess.Core;
using DataAccess.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Implementations.Repositories
{
    public class BookRepository : IBookRepository
    {
        SalesContext _context;

        public BookRepository(SalesContext context)
        {
            _context = context;
        }

        public async Task<long> AddBookAsync(Book item)
        {
            _context.Attach(item.Author);
            var res = await _context.Books.AddAsync(item);
            await _context.SaveChangesAsync();
            return res.Entity.Id;
        }

        public async Task<bool> DeleteBookByIdAsync(long bookId)
        {
            Book book = await _context.Books.FirstOrDefaultAsync(b => b.Id == bookId);
            if (book == null)
                return false;

            _context.Books.Remove(book);
            await _context.SaveChangesAsync();
            return true;
        }

        public IQueryable<Book> GetAllBooksAsQueryable()
        {
            var res = _context.Books
                .Include(b => b.Author)
                .AsQueryable();
            return res;
        }

        public async Task<Book> GetBookByIdAsync(long id)
        {
            var res = await _context.Books
                .Include(b => b.Author)
                .FirstOrDefaultAsync(b => b.Id == id);
            return res;
        }

        public async Task<List<Book>> GetBooksPagedAsync(int pageNum, int batchSize)
        {
            var res = await _context.Books
                .Include(b => b.Author)
                .OrderBy(b => b.Title)
                .Skip((pageNum - 1) * batchSize)
                .Take(batchSize)
                .ToListAsync();

            return res;
        }

        public async Task<int> GetTotalBooksCountAsync()
        {
            var res = await _context.Books.CountAsync();
            return res;
        }

        public async Task<bool> UpdateAsync(Book item)
        {
            try
            {
                var res = _context.Books.Update(item);
                await _context.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
