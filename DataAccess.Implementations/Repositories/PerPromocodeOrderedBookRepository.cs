﻿using DataAccess.Abstractions.Repositories;
using DataAccess.Core;
using DataAccess.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Implementations.Repositories
{
    public class PerPromocodeOrderedBookRepository : IPerPromocodeOrderedBookRepository
    {
        SalesContext _context;

        public PerPromocodeOrderedBookRepository(SalesContext context)
        {
            _context = context;
        }

        public async Task<bool> CheckIfBookAlreadyOrderedAsync(long bookId, string promoCodeValue)
        {
            var res = await _context.PerPromocodeOrderedBooks
                .Include(p => p.PromoCode)
                .AnyAsync(p => p.BookId == bookId && p.PromoCode.Value == promoCodeValue);

            return res;
        }

        public async Task<PerPromocodeOrderedBook> CreateAsync(PerPromocodeOrderedBook item)
        {
            var res = await _context.PerPromocodeOrderedBooks.AddAsync(item);
            await _context.SaveChangesAsync();
            return res.Entity;
        }

        public async Task<bool> DeleteItemAsync(PerPromocodeOrderedBook item)
        {
            var res = _context.PerPromocodeOrderedBooks.Remove(item);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<List<Book>> GetBooksByPromocodeValueAsync(string promoCodeValue)
        {
            var res = await _context.PerPromocodeOrderedBooks
                .Include(p => p.Book)
                .ThenInclude(b => b.Author)
                .Include(p => p.PromoCode)
                .Where(p => p.PromoCode.Value == promoCodeValue)
                .Select(p => p.Book)
                .ToListAsync();

            return res;
        }

        public async Task<PerPromocodeOrderedBook> GetOrderEntryByBookIdAndPromocodeValueAsync(long bookId, string promoCodeValue)
        {
            var res = await _context.PerPromocodeOrderedBooks
                .Include(p => p.PromoCode)
                .Include(p => p.Book)
                .FirstOrDefaultAsync(p => p.BookId == bookId && p.PromoCode.Value == promoCodeValue);
            return res;
        }
    }
}
