﻿using DataAccess.Abstractions.Repositories;
using DataAccess.Core;
using DataAccess.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Implementations.Repositories
{
    public class PromoCodeRepository : IPromoCodeRepository
    {
        SalesContext _context;

        public PromoCodeRepository(SalesContext context)
        {
            _context = context;
        }

        public async Task<PromoCode> GetPromoCodeByValueAsync(string value)
        {
            var res =  await _context.PromoCodes.FirstOrDefaultAsync(x => x.Value == value);
            return res;
        }

        public async Task<PromoCode> AddNewPromoCodeAsync(PromoCode model)
        {
            var res = await _context.PromoCodes.AddAsync(model);
            await _context.SaveChangesAsync();
            return res.Entity;
        }

        public async Task<bool> UpdateAsync(PromoCode item)
        {
            var res = _context.PromoCodes.Update(item);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
