﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DataAccess.Core.Models
{
    public partial class Book
    {
        public Book()
        {
            PerPromocodeOrderedBooks = new HashSet<PerPromocodeOrderedBook>();
        }

        public long Id { get; set; }
        public string Title { get; set; }
        public long AuthorId { get; set; }
        public string Isbn { get; set; }
        public string CoverImageFileName { get; set; }
        public decimal Price { get; set; }
        public int QuantityLeft { get; set; }

        public virtual Author Author { get; set; }
        public virtual ICollection<PerPromocodeOrderedBook> PerPromocodeOrderedBooks { get; set; }
    }
}
