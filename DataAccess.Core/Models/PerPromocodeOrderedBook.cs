﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DataAccess.Core.Models
{
    public partial class PerPromocodeOrderedBook
    {
        public long Id { get; set; }
        public long PromoCodeId { get; set; }
        public long BookId { get; set; }

        public virtual Book Book { get; set; }
        public virtual PromoCode PromoCode { get; set; }
    }
}
