﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DataAccess.Core.Models
{
    public partial class PromoCode
    {
        public PromoCode()
        {
            PerPromocodeOrderedBooks = new HashSet<PerPromocodeOrderedBook>();
        }

        public long Id { get; set; }
        public string Value { get; set; }
        public bool Used { get; set; }

        public virtual ICollection<PerPromocodeOrderedBook> PerPromocodeOrderedBooks { get; set; }
    }
}
