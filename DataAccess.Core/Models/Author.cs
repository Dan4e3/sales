﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DataAccess.Core.Models
{
    public partial class Author
    {
        public Author()
        {
            Books = new HashSet<Book>();
        }

        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}
