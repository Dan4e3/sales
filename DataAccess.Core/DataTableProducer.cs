﻿using DataAccess.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Core
{
    public  static class DataTableProducer
    {
        public static DataTable CreateDataTable<T>(string tableName, IEnumerable<T> data) where T : class
        {
            DataTable resDt = new DataTable(tableName);

            var typeProps = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);

            foreach (var prop in typeProps)
            {
                var nameAttr = prop.GetCustomAttribute<ColumnDisplayNameAttribute>();
                string columnName = nameAttr == null ? prop.Name : nameAttr.Name;
                resDt.Columns.Add(columnName, prop.PropertyType);
            }

            object[] values = new object[typeProps.Length];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = typeProps[i].GetValue(item);
                }
                resDt.Rows.Add(values);
            }

            return resDt;
        }
    }
}
