﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using DataAccess.Core.Models;

#nullable disable

namespace DataAccess.Core
{
    public partial class SalesContext : DbContext
    {
        public SalesContext()
        {
        }

        public SalesContext(DbContextOptions<SalesContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Author> Authors { get; set; }
        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<PerPromocodeOrderedBook> PerPromocodeOrderedBooks { get; set; }
        public virtual DbSet<PromoCode> PromoCodes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Cyrillic_General_CI_AS");

            modelBuilder.Entity<Author>(entity =>
            {
                entity.HasIndex(e => new { e.LastName, e.FirstName, e.MiddleName }, "FullName_Unique_Index")
                    .IsUnique();

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.MiddleName)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Book>(entity =>
            {
                entity.HasIndex(e => new { e.Title, e.AuthorId }, "Title_AuthorId_Unique_Index")
                    .IsUnique();

                entity.Property(e => e.CoverImageFileName)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.Isbn)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("ISBN");

                entity.Property(e => e.Price).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(300);

                entity.HasOne(d => d.Author)
                    .WithMany(p => p.Books)
                    .HasForeignKey(d => d.AuthorId)
                    .HasConstraintName("FK_Books_Authors");
            });

            modelBuilder.Entity<PerPromocodeOrderedBook>(entity =>
            {
                entity.HasIndex(e => e.PromoCodeId, "PromocodeId_Index");

                entity.HasOne(d => d.Book)
                    .WithMany(p => p.PerPromocodeOrderedBooks)
                    .HasForeignKey(d => d.BookId)
                    .HasConstraintName("FK_PerPromocodeOrderedBooks_Books");

                entity.HasOne(d => d.PromoCode)
                    .WithMany(p => p.PerPromocodeOrderedBooks)
                    .HasForeignKey(d => d.PromoCodeId)
                    .HasConstraintName("FK_PerPromocodeOrderedBooks_PromoCodes");
            });

            modelBuilder.Entity<PromoCode>(entity =>
            {
                entity.HasIndex(e => e.Value, "Value_Unique_Index")
                    .IsUnique();

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
