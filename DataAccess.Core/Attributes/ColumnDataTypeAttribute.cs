﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Core.Attributes
{
    public class ColumnDataTypeAttribute: Attribute
    {
        public string TypeName { get; set; }
        
        public ColumnDataTypeAttribute(string typeName)
        {
            TypeName = typeName;
        }
    }
}
