﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Core.Attributes
{
    public class ColumnDisplayNameAttribute: Attribute
    {
        public string Name { get; set; }
        
        public ColumnDisplayNameAttribute(string name)
        {
            Name = name;
        }
    }
}
