﻿CREATE TABLE [dbo].[Authors] (
    [Id]         BIGINT         IDENTITY (1, 1) NOT NULL,
    [FirstName]  NVARCHAR (100) NOT NULL,
    [LastName]   NVARCHAR (100) NOT NULL,
    [MiddleName] NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_Authors] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [FullName_Unique_Index]
    ON [dbo].[Authors]([LastName] ASC, [FirstName] ASC, [MiddleName] ASC);

