﻿CREATE TABLE [dbo].[PromoCodes] (
    [Id]    BIGINT         IDENTITY (1, 1) NOT NULL,
    [Value] NVARCHAR (100) NOT NULL,
    [Used]  BIT            NOT NULL,
    CONSTRAINT [PK_PromoCodes] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [Value_Unique_Index]
    ON [dbo].[PromoCodes]([Value] ASC);

