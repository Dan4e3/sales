﻿CREATE TABLE [dbo].[Books] (
    [Id]                 BIGINT          IDENTITY (1, 1) NOT NULL,
    [Title]              NVARCHAR (300)  NOT NULL,
    [AuthorId]           BIGINT          NOT NULL,
    [ISBN]               NVARCHAR (100)  NOT NULL,
    [CoverImageFileName] NVARCHAR (256)  NOT NULL,
    [Price]              DECIMAL (10, 2) NOT NULL,
    [QuantityLeft]       INT             NOT NULL,
    CONSTRAINT [PK_Books] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Books_Authors] FOREIGN KEY ([AuthorId]) REFERENCES [dbo].[Authors] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [Title_AuthorId_Unique_Index]
    ON [dbo].[Books]([Title] ASC, [AuthorId] ASC);

