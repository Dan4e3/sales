﻿CREATE TABLE [dbo].[PerPromocodeOrderedBooks] (
    [Id]          BIGINT IDENTITY (1, 1) NOT NULL,
    [PromoCodeId] BIGINT NOT NULL,
    [BookId]      BIGINT NOT NULL,
    CONSTRAINT [PK_PerPromocodeOrderedBooks] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PerPromocodeOrderedBooks_Books] FOREIGN KEY ([BookId]) REFERENCES [dbo].[Books] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_PerPromocodeOrderedBooks_PromoCodes] FOREIGN KEY ([PromoCodeId]) REFERENCES [dbo].[PromoCodes] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [PromocodeId_Index]
    ON [dbo].[PerPromocodeOrderedBooks]([PromoCodeId] ASC);

