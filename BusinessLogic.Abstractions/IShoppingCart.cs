﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Abstractions
{
    public interface IShoppingCart<T> where T: class
    {
        Task<List<T>> AddToShoppingCartAsync(long itemId);
        List<T> RemoveFromShoppingCart(long itemId);
    }
}
